FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD /build/libs/statistics-service-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
EXPOSE 8080/tcp
EXPOSE 80/tcp
EXPOSE 8280/tcp
EXPOSE 8281/tcp
EXPOSE 8282/tcp
