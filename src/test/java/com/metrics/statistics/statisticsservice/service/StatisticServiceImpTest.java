package com.metrics.statistics.statisticsservice.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class StatisticServiceImpTest {

    @Autowired
    private StatisticsService statisticsService;

    @DisplayName("find by name!!")
    @Test
    void getByName() {
        Assertions.assertEquals("sandeep.ojha930", statisticsService.
                findByAccountName("sandeep.ojha930").get(0).getId().getAccount());
    }
}
