package com.metrics.statistics.statisticsservice.domain.timeseries;

import com.metrics.statistics.statisticsservice.enums.Currency;
import com.metrics.statistics.statisticsservice.enums.StatisticMetric;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;
import java.util.Set;

@Data
@Document(collection = "datapoints")
public class DataPoint implements Serializable {

    @Id
    private DataPointId id;

    private Set<ItemMetric> incomes;

    private Set<ItemMetric> expenses;

    private Map<StatisticMetric, BigDecimal> statistics;

    private Map<Currency, BigDecimal> rates;

}
