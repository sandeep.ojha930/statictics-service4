package com.metrics.statistics.statisticsservice.domain.timeseries;

import lombok.Getter;

import java.math.BigDecimal;

@Getter
public class ItemMetric {

    private String title;

    private BigDecimal amount;

    public ItemMetric(String title, BigDecimal amount) {
        this.title = title;
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemMetric that = (ItemMetric) o;
        return title.equalsIgnoreCase(that.title);
    }

    @Override
    public int hashCode() {
        return title.hashCode();
    }
}
