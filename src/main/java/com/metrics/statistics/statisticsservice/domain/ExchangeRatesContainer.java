package com.metrics.statistics.statisticsservice.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.metrics.statistics.statisticsservice.enums.Currency;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true, value = {"date"})
public class ExchangeRatesContainer {

    private LocalDate date = LocalDate.now();

    private Currency base;

    private Map<String, BigDecimal> rates;

    @Override
    public String toString() {
        return "RateList{" +
                "date=" + date +
                ", base=" + base +
                ", rates=" + rates +
                '}';
    }
}
