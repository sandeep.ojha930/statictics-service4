package com.metrics.statistics.statisticsservice.enums;

public enum Currency {

	USD, EUR, RUB;

	public static Currency getBase() {
		return USD;
	}
}
