package com.metrics.statistics.statisticsservice.enums;

public enum StatisticMetric {

	INCOMES_AMOUNT, EXPENSES_AMOUNT, SAVING_AMOUNT

}
