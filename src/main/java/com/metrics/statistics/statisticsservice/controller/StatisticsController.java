package com.metrics.statistics.statisticsservice.controller;

import com.metrics.statistics.statisticsservice.domain.Account;
import com.metrics.statistics.statisticsservice.domain.timeseries.DataPoint;
import com.metrics.statistics.statisticsservice.service.StatisticsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RequestMapping("/statistics")
@RestController
public class StatisticsController {

    @Autowired
    private StatisticsService statisticsService;

    @GetMapping("/{accountName}")
    public ResponseEntity<List<DataPoint>> getStatisticsByAccountName(@PathVariable String accountName) {
        log.info("Fetch the statistics by account: " + accountName);
        return new ResponseEntity<>(statisticsService.findByAccountName(accountName), HttpStatus.OK);
    }

    @PutMapping("/{accountName}")
    public ResponseEntity<String> saveAccountStatistics(@PathVariable String accountName, @Valid @RequestBody Account account) {
        String message;
        HttpStatus httpStatus;
        try {
            statisticsService.save(accountName, account);
            message = "Successfully Updated !!";
            httpStatus = HttpStatus.ACCEPTED;
        } catch (Exception e) {
            message = "Getting failed:" + e.getMessage();
            httpStatus = HttpStatus.BAD_REQUEST;
            log.error("An errored occurred in saveAccountStatistics method: " + e.getMessage());
        }
        log.info("Data is saved successfully for account: " + accountName);
        return new ResponseEntity<>(message, httpStatus);
    }
}
