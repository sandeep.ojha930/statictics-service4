package com.metrics.statistics.statisticsservice.repository;

import com.metrics.statistics.statisticsservice.domain.timeseries.DataPoint;
import com.metrics.statistics.statisticsservice.domain.timeseries.DataPointId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DataPointRepository extends MongoRepository<DataPoint, DataPointId> {
    List<DataPoint> findByIdAccount(String account);
}
